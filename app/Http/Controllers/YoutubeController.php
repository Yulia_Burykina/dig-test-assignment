<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\PopularVideosInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class YoutubeController extends Controller
{
    private $videosService;

    /**
     * Create a new controller instance.
     *
     * @param PopularVideosInterface $videosService
     *
     * @return void
     */
    public function __construct(PopularVideosInterface $videosService)
    {
        $this->videosService = $videosService;
    }

    /**
     * Returns lists of most popular Youtube videos for given countries along with
     * Wikipedia preview for each country
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return JsonResponse
     */
    public function popular(Request $request): JsonResponse
    {
        $offset = (int) $request->get('offset', 0);
        $pageSize = (int) $request->get('pagesize', 0);
        try {
            return new JsonResponse($this->videosService->getForCountries($offset, $pageSize));
        } catch (\Exception $e) {
            return new JsonResponse('Error: ' . $e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
