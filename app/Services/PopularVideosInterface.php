<?php

declare(strict_types=1);

namespace App\Services;

interface PopularVideosInterface {

    const COUNTRIES = [
        'gb' => 'United Kingdom',
        'nl' => 'Netherlands',
        'de' => 'Germany',
        'fr' => 'France',
        'es' => 'Spain',
        'it' => 'Italy',
        'gr' => 'Greece'
    ];

    /**
     * @param int $offset
     * @param int $pagesize
     * @return array
     */
    public function getForCountries(int $offset, int $pagesize): array;
}