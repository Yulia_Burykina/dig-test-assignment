<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Contracts\Cache\Repository;

class YoutubePopularVideosService implements PopularVideosInterface
{
    private const MINUTES_TO_CACHE_YOUTUBE = 5;

    private $cache;

    private $httpClient;

    private $youtubeUrl;

    public function __construct(Repository $cache, BatchHttpClientInterface $httpClient, string $youtubeUrl)
    {
        $this->cache = $cache;
        $this->httpClient = $httpClient;
        $this->youtubeUrl = $youtubeUrl;
    }

    /**
     * Gets popular videos for countries from Youtube API
     *
     * @param int $offset
     * @param int $pagesize
     * @return array
     * @throws \Exception
     */
    public function getForCountries(int $offset, int $pagesize): array
    {
        if ($pagesize === 0) {
            $pagesize = null;
        }
        $requestedCountries = array_slice(self::COUNTRIES, $offset, $pagesize);

        $urlsToQuery = [];
        $result = [];
        foreach ($requestedCountries as $code => $title) {
            if ($this->cache->has($code)) {
                $result[$code] = json_decode($this->cache->get($code));
                continue;
            }
            $urlsToQuery[$code] = [
                'baseUrl' => $this->youtubeUrl,
                'params' => ['countryCode' => $code]
            ];
        }

        if (!$urlsToQuery) {
            return $result;
        }

        $queryResults = $this->httpClient->get($urlsToQuery);

        foreach ($queryResults as $code => $queryResult) {
            $result[$code] = json_decode($queryResult);
            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new \Exception('Invalid result for ' . self::COUNTRIES[$code]);
            }

            $this->cache->put($code, $queryResult, self::MINUTES_TO_CACHE_YOUTUBE);
        }

        uksort($result, [$this, 'sortCountryCodes']);

        return $result;
    }

    private function sortCountryCodes(string $a, string $b): int
    {
        $codes = array_keys(self::COUNTRIES);
        return array_search($a, $codes) <=> array_search($b, $codes);
    }
}