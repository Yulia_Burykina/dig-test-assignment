<?php

declare(strict_types=1);

namespace App\Services;

interface BatchHttpClientInterface
{
    /**
     * @param array $queries
     * @return array
     */
    public function get(array $queries): array;
}