<?php

declare(strict_types=1);

namespace App\Services;

class ParallelHttpClient implements BatchHttpClientInterface
{
    /**
     * Concurrently executes queries using curl_multi
     *
     * @param array $queries
     * @return array
     * @throws \Exception
     */
    public function get(array $queries): array
    {
        $urls = [];
        foreach ($queries as $key => $query) {
            if (!is_array($query['params'])) {
                $query['params'] = [];
            }
            $urls[$key] = $this->getUrl($query['baseUrl'], $query['params']);
        }
        return $this->query($urls);
    }

    /**
     * @throws \Exception
     */
    protected function getUrl(string $baseUrl, array $params): string
    {
        preg_match_all('/\\{(\w+)\\}/', $baseUrl, $matches);

        $url = $baseUrl;

        foreach ($matches[0] as $key => $match) {
            $paramName = $matches[1][$key];
            if (!array_key_exists($paramName, $params)) {
                throw new \Exception('Some required URL parameters are missing.');
            }
            $url = str_replace($match, urlencode($params[$paramName]), $url);
        }

        return $url;
    }

    /**
     * @throws \Exception
     */
    protected function query(array $urls): array
    {
        $mh = curl_multi_init();

        foreach ($urls as $key => $url) {
            $conn[$key] = curl_init($url);
            curl_setopt($conn[$key], CURLOPT_RETURNTRANSFER, 1);
            curl_multi_add_handle($mh, $conn[$key]);
        }

        do {
            $status = curl_multi_exec($mh, $active);
            $info = curl_multi_info_read($mh);
            //TODO: elaborate exceptions depending on exact error type
            if (is_array($info) && $info['result'] !== CURLE_OK) {
                throw new \Exception('Something went wrong while querying external APIs.');
            }
        } while ($status === CURLM_CALL_MULTI_PERFORM || $active);

        $res = [];
        foreach ($urls as $key => $url) {
            $res[$key] = curl_multi_getcontent($conn[$key]);
            curl_close($conn[$key]);
        }

        return $res;
    }
}