<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Contracts\Cache\Repository;

class WikiPopularVideosServiceDecorator implements PopularVideosInterface
{
    private const MINUTES_TO_CACHE_WIKI = 24 * 60;

    private $cache;

    private $httpClient;

    private $wikiUrl;

    private $wrappee;

    public function __construct(
        Repository $cache,
        BatchHttpClientInterface $httpClient,
        string $wikiUrl,
        PopularVideosInterface $wrappee
    )
    {
        $this->cache = $cache;
        $this->httpClient = $httpClient;
        $this->wikiUrl = $wikiUrl;
        $this->wrappee = $wrappee;
    }

    /**
     * Decorator for YoutubePopularVideosService.
     * Gets popular videos for countries from Youtube API and
     * enriches them from Wikipedia foreword for each country
     *
     * @param int $offset
     * @param int $pagesize
     * @return array
     * @throws \Exception
     */
    public function getForCountries(int $offset, int $pagesize): array
    {
        $result = $this->wrappee->getForCountries($offset, $pagesize);

        $wikiEncoded = $this->cache->remember('wiki_countries', self::MINUTES_TO_CACHE_WIKI, function() {
            $wikiUrl = [
                'baseUrl' => $this->wikiUrl,
                'params' => [
                    'countryTitles' => implode('|', self::COUNTRIES)
                ]
            ];
            $wikiInfo = json_decode($this->httpClient->get([$wikiUrl])[0]);
            if (json_last_error() !== JSON_ERROR_NONE ||
                !$wikiInfo->query ||
                !$wikiInfo->query->pages
            ) {
                throw new \Exception('Invalid result for countries from Wikipedia');
            }
            $wikiCountries = [];
            foreach ($wikiInfo->query->pages as $wikiCountry) {
                $code = array_search($wikiCountry->title, self::COUNTRIES);
                $wikiCountries[$code] = $wikiCountry->extract;
            }
            return json_encode($wikiCountries);
        });
        $wiki = json_decode($wikiEncoded);

        foreach ($result as $code => &$country) {
            $country->wiki_preview = $wiki->{$code};
        }

        return $result;
    }
}