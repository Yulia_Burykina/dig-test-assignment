<?php

namespace App\Providers;

use App\Services\BatchHttpClientInterface;
use App\Services\WikiPopularVideosServiceDecorator;
use App\Services\YoutubePopularVideosService;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Support\ServiceProvider;
use App\Services\PopularVideosInterface;
use App\Services\ParallelHttpClient;
use Illuminate\Contracts\Config\Repository as ConfigRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            BatchHttpClientInterface::class,
            ParallelHttpClient::class
        );

        $this->app->singleton(PopularVideosInterface::class, function ($app) {
            $cache = $app->make(Repository::class);
            $httpClient = $app->make(BatchHttpClientInterface::class);
            $config = $app->make(ConfigRepository::class);
            $youtubeUrl = $config->get('app.youtube_popular_videos_url');
            $wikiUrl = $config->get('app.wikipedia_previews_url');
            return new WikiPopularVideosServiceDecorator(
                $cache,
                $httpClient,
                $wikiUrl,
                new YoutubePopularVideosService($cache, $httpClient, $youtubeUrl)
            );
        }
        );
    }
}
