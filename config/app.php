<?php

$youtubeAPIKey = 'AIzaSyCiYYGAuOmatH9UU449UyVp7kjVjwfR5UE';

return [

    'youtube_api_key' => $youtubeAPIKey,
    'youtube_popular_videos_url' =>
        'https://www.googleapis.com/youtube/v3/videos?' .
        'part=snippet&fields=items(snippet(title,description,thumbnails(default,high)))&' .
        'key=' . $youtubeAPIKey .
        '&chart=mostPopular&regionCode={countryCode}',
    'wikipedia_previews_url' =>
        'https://en.wikipedia.org/w/api.php?format=json&action=query' .
        '&prop=extracts&exintro=1&explaintext=1&titles={countryTitles}'

];
