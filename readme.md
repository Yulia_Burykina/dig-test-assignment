# Test Assignment for DIG

A simple JSON API that fetches the most populat youtube videos for given countries along with Wikipedia foreword for each country. Countries (in order): GB, NL, DE, FR, ES, IT, GR. 

## Example Usage

1. Clone the project code.
2. `composer install`
3. Run dev server: `php -S localhost:8000 -t public`
4. In your browser: `localhost:8000/popular`

#### Offset and page size examples:

`localhost:8000/popular?offset=2` will give you results for Germany and the rest (in order)

`localhost:8000/popular?offset=2&pagesize=3` will give you results for Germany, France and Spain

`localhost:8000/popular?pagesize=2` will give you results for Great Britain and Netherlands

## Testing

In project directory: `./vendor/bin/phpunit`

## Notes

In order not to trigger quota limits with Youtube API I made a simple decision 
to cache responses for every country for a reasonable time. 
If Q is the daily quota (in units), W is the weight of each youtube query (in units)
, N is the number of countries in the application, 
then we should cache each response for no less than `(24*60*W*N)/Q` minutes. 
With Q=1 000 000, N=7, W=3 the formula gives us about 0.03 minutes.
 
In the end I decided that 5 minutes is a reasonable time for caching popular videos,
and 1 day is a good caching time for Wikipedia foreword about countries. These number are 
pretty random and of course can be futher adjusted because there is not much context about 
this application now. 

This assignment is a bit raw because I wanted to stick to rational time limits. 
The following improvements should have been done for a proper project:

* More elaborate exception classes instead of just basic php Exceptions.
* Integration tests for `/popular` endpoint with HttpClient stub 
  (to do that I would have to dump a lot of data from Youtube, 
  Wikipedia and the endpoint into files, then parse them back to code 
  which would be rather time consuming but of course in a real project it's essential).

