<?php

use App\Services\ParallelHttpClient;
use App\Services\YoutubePopularVideosService;
use Illuminate\Contracts\Cache\Repository;
use App\Services\PopularVideosInterface;

class YoutubePopularVideosServiceTest extends TestCase
{
    public function testGetForCountries()
    {
        $clientStub = $this->createMock(ParallelHttpClient::class);

        $clientStub->method('get')
            ->will($this->returnCallback([$this, 'getCorrectAPIResults']));

        $service = new YoutubePopularVideosService(
            $this->app->make(Repository::class),
            $clientStub,
            'youtube_url'
        );

        //@dataProvider is not used here because we need to use the same cache storage
        //for all datasets to make sure that they are sorted to match original
        //country order during each request, even when some countries are extracted
        //from cache and some from external API
        $args = $this->correctResultsProvider();
        foreach ($args as $argSet) {
            $videos = $service->getForCountries($argSet[0], $argSet[1]);
            $this->assertEquals($argSet[2], $videos);
        }
    }

    public function testGetForCountriesJsonError()
    {
        $clientStub = $this->createMock(ParallelHttpClient::class);

        $clientStub->method('get')
            ->will($this->returnCallback([$this, 'getIncorrectAPIResults']));

        $service = new YoutubePopularVideosService(
            $this->app->make(Repository::class),
            $clientStub,
            'youtube_url'
        );

        try {
            $service->getForCountries(0, 1);
        } catch (\Exception $e) {
            $this->assertStringStartsWith('Invalid result for', $e->getMessage());
            return;
        }
        $this->fail('Exception is not raised.');
    }

    public function getCorrectAPIResults(array $urls): array
    {
        $results = [];
        foreach ($urls as $code => $url) {
            //correct API results should be valid JSON
            $results[$code] = json_encode(PopularVideosInterface::COUNTRIES[$code]);
        }
        return $results;
    }

    public function getIncorrectAPIResults(array $urls): array
    {
        $results = [];
        foreach ($urls as $code => $url) {
            $results[$code] = 'invalid json';
        }
        return $results;
    }

    protected function correctResultsProvider()
    {
        $args = [
            [2, 2],
            [0, 3],
            [5, 0],
            [0, 0]
        ];
        foreach ($args as &$argSet) {
            $offset = $argSet[0];
            $pagesize = $argSet[1] === 0 ? null : $argSet[1];
            $result = array_slice(PopularVideosInterface::COUNTRIES, $offset, $pagesize);
            $argSet[2] = $result;
        }
        return $args;
    }
}
