<?php

use App\Services\ParallelHttpClient;
use App\Services\WikiPopularVideosServiceDecorator;
use App\Services\YoutubePopularVideosService;
use Illuminate\Contracts\Cache\Repository;
use App\Services\PopularVideosInterface;

class WikiPopularVideosServiceDecoratorTest extends TestCase
{

    public function testGetForCountries()
    {
        $clientStub = $this->createMock(ParallelHttpClient::class);

        $clientStub->method('get')
            ->will($this->returnCallback([$this, 'getCorrectAPIResults']));

        $youtubeServiceStub = $this->createMock(YoutubePopularVideosService::class);

        $youtubeServiceStub->method('getForCountries')
            ->will($this->returnCallback([$this, 'getYoutubeResults']));

        $service = new WikiPopularVideosServiceDecorator(
            $this->app->make(Repository::class),
            $clientStub,
            'wiki_url',
            $youtubeServiceStub
        );

        //@dataProvider is not used here because we need to use the same cache storage
        //for all datasets to make sure that they are sorted to match original
        //country order during each request, even when some countries are extracted
        //from cache and some from external API
        $args = $this->correctResultsProvider();
        foreach ($args as $argSet) {
            $videos = $service->getForCountries($argSet[0], $argSet[1]);
            $this->assertEquals(count($argSet[2]), count($videos));
            foreach ($argSet[2] as $code => $object) {
                $this->assertEquals($object->items, $videos[$code]->items);
                $this->assertEquals($object->wiki_preview, $videos[$code]->wiki_preview);
            }

        }
    }

    public function testGetForCountriesJsonError()
    {
        $clientStub = $this->createMock(ParallelHttpClient::class);

        $clientStub->method('get')
            ->will($this->returnCallback([$this, 'getIncorrectAPIResults']));

        $youtubeServiceStub = $this->createMock(YoutubePopularVideosService::class);

        $youtubeServiceStub->method('getForCountries')
            ->will($this->returnCallback([$this, 'getYoutubeResults']));

        $service = new WikiPopularVideosServiceDecorator(
            $this->app->make(Repository::class),
            $clientStub,
            'wiki_url',
            $youtubeServiceStub
        );

        try {
            $service->getForCountries(0, 1);
        } catch (\Exception $e) {
            $this->assertEquals('Invalid result for countries from Wikipedia', $e->getMessage());
            return;
        }
        $this->fail('Exception is not raised.');
    }

    public function getCorrectAPIResults(array $urls): array
    {
        //wikipedia API response structure
        $result = new stdClass();
        $result->query = new stdClass();
        $result->query->pages = new stdClass();
        foreach (PopularVideosInterface::COUNTRIES as $code => $title) {
            $result->query->pages->{$code} = new stdClass();
            $result->query->pages->{$code}->title = $title;
            $result->query->pages->{$code}->extract = $code . ' extract';
        }
        return [0 => json_encode($result)];
    }

    public function getYoutubeResults(int $offset, int $pagesize): array
    {
        $results = [];
        if ($pagesize === 0) {
            $pagesize = null;
        }
        $countries = array_slice(PopularVideosInterface::COUNTRIES, $offset, $pagesize);
        foreach ($countries as $code => $title) {
            $results[$code] = new stdClass();
            $results[$code]->items = 'some items for ' . $code;
        }
        return $results;
    }

    protected function correctResultsProvider()
    {
        $args = [
            [2, 2],
            [0, 3],
            [5, 0],
            [0, 0]
        ];
        foreach ($args as &$argSet) {
            $offset = $argSet[0];
            $pagesize = $argSet[1] === 0 ? null : $argSet[1];
            $countries = array_slice(PopularVideosInterface::COUNTRIES, $offset, $pagesize);
            $result = [];
            foreach ($countries as $code => $title) {
                $result[$code] = new stdClass();
                $result[$code]->items = 'some items for ' . $code;
                $result[$code]->wiki_preview = $code . ' extract';
            }
            $argSet[2] = $result;
        }
        return $args;
    }

    public function getIncorrectAPIResults(array $urls): array
    {
        return [0 => 'invalid json'];
    }
}
